# frozen_string_literal: true

require_relative "lib/range_list/version"

Gem::Specification.new do |spec|
  spec.name = "range_list"
  spec.version = RangeList::VERSION
  spec.authors = ["Song Huang"]
  spec.email = ["songhuangcn@gmail.com"]

  spec.summary = "RangeList is a library that can handle ranges quickly and appropriately."
  # spec.description = "TODO: Write a longer description or delete this line."
  spec.homepage = "https://gitlab.com/songhuangcn/range_list"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  # spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/songhuangcn/range_list"
  spec.metadata["documentation_uri"] = "https://rubydoc.info/gems/range_list"
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  # spec.bindir = "exe"
  # spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"
  spec.add_dependency "rbtree", "~> 0.4.5"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
