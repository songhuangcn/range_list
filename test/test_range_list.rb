# frozen_string_literal: true

require "test_helper"

class TestRangeList < Minitest::Test
  def setup
    @range_list = RangeList.new
  end

  def test_argument_error_not_array
    assert_raises(RangeList::ArgumentError) { @range_list.add("") }
    assert_raises(RangeList::ArgumentError) { @range_list.remove("") }
    assert_raises(RangeList::ArgumentError) { @range_list.contains_all?("") }
    assert_raises(RangeList::ArgumentError) { @range_list.contains_any?("") }
  end

  def test_argument_error_not_tow_elements
    assert_raises(RangeList::ArgumentError) { @range_list.add([2022]) }
    assert_raises(RangeList::ArgumentError) { @range_list.remove([2022]) }
    assert_raises(RangeList::ArgumentError) { @range_list.contains_all?([2022]) }
    assert_raises(RangeList::ArgumentError) { @range_list.contains_any?([2022]) }
  end

  def test_argument_error_not_all_integer
    assert_raises(RangeList::ArgumentError) { @range_list.add([2022, ""]) }
    assert_raises(RangeList::ArgumentError) { @range_list.remove([2022, ""]) }
    assert_raises(RangeList::ArgumentError) { @range_list.contains_all?([2022, ""]) }
    assert_raises(RangeList::ArgumentError) { @range_list.contains_any?([2022, ""]) }
  end

  def test_argument_error_not_integer
    assert_raises(RangeList::ArgumentError) { @range_list.contains?([]) }
  end

  def test_range_list
    @range_list.add([1, 5])
    assert_output("[1, 5)\n") { @range_list.print }

    @range_list.add([10, 20])
    assert_output("[1, 5) [10, 20)\n") { @range_list.print }

    @range_list.add([20, 20])
    assert_output("[1, 5) [10, 20)\n") { @range_list.print }

    @range_list.add([20, 21])
    assert_output("[1, 5) [10, 21)\n") { @range_list.print }

    @range_list.add([2, 4])
    assert_output("[1, 5) [10, 21)\n") { @range_list.print }

    @range_list.add([3, 8])
    assert_output("[1, 8) [10, 21)\n") { @range_list.print }

    @range_list.remove([10, 10])
    assert_output("[1, 8) [10, 21)\n") { @range_list.print }

    @range_list.remove([10, 11])
    assert_output("[1, 8) [11, 21)\n") { @range_list.print }

    @range_list.remove([15, 17])
    assert_output("[1, 8) [11, 15) [17, 21)\n") { @range_list.print }

    @range_list.remove([3, 19])
    assert_output("[1, 3) [19, 21)\n") { @range_list.print }

    assert @range_list.contains_any?([10, 20])
    assert !@range_list.contains_any?([10, 19])
    assert !@range_list.contains_all?([10, 20])
    assert !@range_list.contains?(3)
    assert @range_list.contains?(19)

    data = [[1, 3], [19, 21]]
    @range_list.each_with_index do |(range_start, range_end), index|
      assert_equal data[index][0], range_start
      assert_equal data[index][1], range_end
    end

    @range_list.remove([0, 100])
    assert_output("\n") { @range_list.print }
  end
end
