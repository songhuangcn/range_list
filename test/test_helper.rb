# frozen_string_literal: true

require "simplecov"
SimpleCov.start do
  add_filter "vendor" # Don't include vendored stuff
end

if ENV["CI"]
  require "simplecov-cobertura"
  SimpleCov.formatter = SimpleCov::Formatter::CoberturaFormatter
end

$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "range_list"

require "minitest/autorun"
