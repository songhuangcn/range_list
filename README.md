# RangeList

RangeList is a library that can handle ranges quickly and appropriately.

A pair of integers define a range, for example: [1, 5), this range includes integers: 1, 2, 3, and 4.
A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201).

RangeList use AVL Tree processing internally, so range operations are very fast.

[![Gem Version](https://badge.fury.io/rb/range_list.svg)](https://rubygems.org/gems/range_list)
[![Documentation](https://img.shields.io/badge/docs-YARD-blue.svg)](https://rubydoc.info/gems/range_list)
[![pipeline status](https://gitlab.com/songhuangcn/range_list/badges/main/pipeline.svg)](https://gitlab.com/songhuangcn/range_list/-/commits/main)

## Installation

Add gem to your application's Gemfile:

```shell
bundle add range_list
```

Or install it yourself as:

```shell
gem install range_list
```

## Usage

```shell
range_list = RangeList.new
range_list.add([1, 5]).add([10, 20])
range_list.print # [1, 5) [10, 20)

range_list.add([20, 20])
range_list.print # [1, 5) [10, 20)

range_list.add([15, 21])
range_list.print # [1, 5) [10, 21)

range_list.remove([15, 21])
range_list.print # [1, 5) [10, 15)

range_list.contains_any?([8, 10]) # false
range_list.contains_any?([8, 11]) # true
range_list.contains_all?([8, 11]) # false
range_list.contains_all?([10, 11]) # true
range_list.contains?(5) # false
range_list.contains?(10) # true

# RangeList has the `Enumerable` ability, feel free to use `each`, `map`, `to_a`, `min`, `max` etc. 
range_list.each { |range_start, range_end| puts "#{range_start} <= x < #{range_end}" }

range_list.remove([0, 100])
range_list.print #
range_list.to_a # []
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and Merge requests are welcome on GitLab at https://gitlab.com/songhuangcn/range_list.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
