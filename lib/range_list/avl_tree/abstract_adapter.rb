# frozen_string_literal: true

class RangeList
  # @!visibility private
  module AvlTree
    class AbstractAdapter
      def put(key, value)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      def lower_entry(key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      def sub_map(from_key, to_key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      def remove(key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      def each(&block)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end
    end
  end
end
