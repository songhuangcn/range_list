# frozen_string_literal: true

require "rbtree"
require_relative "abstract_adapter"

class RangeList
  # @!visibility private
  module AvlTree
    class RBTreeAdapter < AbstractAdapter
      def initialize
        @rbtree = RBTree.new
      end

      def put(key, value)
        rbtree[key] = value
      end

      def lower_entry(key)
        rbtree.upper_bound(key)
      end

      def sub_map(from_key, to_key)
        rbtree.bound(from_key, to_key).to_a
      end

      def remove(key)
        rbtree.delete(key)
      end

      def each(&block)
        rbtree.each(&block)
      end

      private

      attr_reader :rbtree
    end
  end
end
