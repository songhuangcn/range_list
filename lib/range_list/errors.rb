class RangeList
  # RangeList base error.
  class Error < StandardError; end

  # When the error raised, you should see the doc and check your argument.
  class ArgumentError < Error; end
end
